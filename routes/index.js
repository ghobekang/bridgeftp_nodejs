var express = require('express');
var ftp = require('ftp');
var bodyParser = require('body-parser');
var Multer = require('multer');
var upload = new Multer({dest: '/Volumes/Data/temp/'}).any();
var router = express.Router();
var fs = require('fs');
var crypto = require('crypto');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/ftp-connection', upload, function(req, res, next) {
  var files = req.files;
  var client = new ftp();
  var count = 0;
  var hashcode = Math.random().toString(36).substring(7);

  client.on('ready', function() {
    client.mkdir('/data/skin/front/DishInside/img/template/' + hashcode, false, function(err) {
      if (err) throw err;
      for (var i = 0; i < files.length; i += 1) {
        client.put(files[i].path, '/data/skin/front/DishInside/img/template/'+ hashcode + '/' + files[i].fieldname, function(err_put) {
          if (err_put) throw err_put;
          count += 1;
        });
      }
      client.list('/data/skin/front/DishInside/img/template/' + hashcode, function(err, list) {
        res.json({list: list.slice(2), code: hashcode});
      });
      client.end();
    });
  }).on('end', function() {
    for (var i = 0; i < files.length; i += 1) {
      fs.unlinkSync(files[i].path);
    }
    const filelist = fs.readdirSync('/Volumes/Data/temp/');
    if (filelist.length !== 0) {
      for (let i = 0; i < filelist.length; i += 1) {
        fs.unlinkSync('/Volumes/Data/temp/' + filelist[i])
      }
    }
  });

  client.connect({
    host: 'yoribogo1.godomall.com',
    port: 21,
    user: 'yoribgftp',
    password: 'roqkfwnd1',
  });

});

module.exports = router;
